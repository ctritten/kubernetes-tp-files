#!/bin/bash

echo "Writing DB_PASSWORD in env..."
if [ -f /run/secrets/mysql/user_password ] ; then
    export DB_PASSWORD="$(tail -n 1 /run/secrets/mysql/user_password)"
fi

echo "Testing DB_PASSWORD in env: [$DB_PASSWORD]."

rm -f /run/apache2/apache2.pid
echo "PassEnv DB_USER DB_PASSWORD DB_DATABASE DB_HOST" > /etc/apache2/conf-available/env.conf
a2enconf env
exec /usr/sbin/apache2ctl -D FOREGROUND

